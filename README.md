# Java Just For Fun

Here I have gathered some Java apps, that I have created during 04.2024 - 06.2024, and probably add some things even later.

Apps are sorted into packages as follows: 
* animalinterface
* datetime
* flight
* flightclasseswithinterface
* inheritance
  * bank
  * company
  * personsandemployees
  * shapesandinheritance
  * vehiclesapp
* io
  * comparedatafromfiles
  * iostreams
* lambdaexpressions
* musiclibrary
* palindromefinder
* shapesandinterface

(Each packages may contain one or more separate apps. Each java Main method file contains the description of the particular app.)

## Run the apps

* Install Java JDK and Maven
* git clone https://gitlab.com/java-remote-ee27-course/worldofjava/javajustforfun.git
* There are multiple separate apps gathered here, all organized under the appropriate packages under: `javajustforfun/src/main/java/org/example`
* To run an app, just open a package and select the Main method of the class. 

## Author of the codes:
Katlin Kalde

## Licensing
Use as you like, no attribution needed.