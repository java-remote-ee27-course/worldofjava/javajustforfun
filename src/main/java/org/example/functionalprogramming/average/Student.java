package org.example.functionalprogramming.average;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.averagingDouble;

/**
 * Java 8 has introduced a sexy new Stream API which makes it possible to solve some data related problems in just a few lines of code. Let's try it out!
 * You have a Student class (see the class declaration below).
 * You have an array of students and you want to get some aggregate data.
 * THE TASK: get the average grade for every department.
 * Implement the method using Java 8 Stream API.
 *
 * I used for solving task
 * https://www.baeldung.com/java-groupingby-collector
 *
 * Also good material:
 * https://javarevisited.blogspot.com/2022/12/how-to-update-value-for-key-hashmap.html
 */
class Aggregation {
    public static Map<String, Double> getAverageGradeByDepartment(Stream<Student> students){
        return students.collect(Collectors.groupingBy(
            Student::getDepartment,
                averagingDouble(Student::getGrade)
        ));
    }
    public static void main(String[] args) {
        //Generate a basic array of students:
        Student galina = new Student("Galina", 95, "Philology");
        Student anton = new Student("Anton", 90, "CS");
        Student jack = new Student("Jack", 82, "Philology");
        Student mike = new Student("Mike", 60, "Philology");
        Student jane = new Student("Jane", 65, "CS");

        Student[] students = new Student[] {galina, anton, jack, mike, jane};
        Map<String, Double> averagesByDept = getAverageGradeByDepartment(Arrays.stream(students));
        System.out.println(averagesByDept);
    }
}

@Getter
@Setter
public class Student {
    private String name;
    private double grade;
    private String department;

    public Student(String name, double grade, String department) {
        this.name = name;
        this.grade = grade;
        this.department = department;
    }
}