package org.example.functionalprogramming;

import java.lang.reflect.Field;
import java.util.function.Function;

/*
 * (NB! The solution of the next task also contains a demo how to use reflections in order to compare private fields)
 * With the release of Java 8, Java has finally added support for "lambda functions", that is,
 * variables that contain a function which can operate on data just as class methods can (well, not just as class methods can...)
 *
 * Function<MyObject, String> f = p -> p.toString();
 * String myString = f.apply(myObject); //Stores whatever the toString() of myObject is in myString
 * The above is a simple mapper function: given an input of type MyObject, return a specific result of type String,
 * in this case the toString of the object.
 * They can, of course, become much more complicated.
 * A full listing of the default function types can be found at
 * http://docs.oracle.com/javase/8/docs/api/java/util/function/package-summary.html

    Given this POJO:

    public class Student {
      private final String firstName;
      private final String lastName;
      public final String studentNumber;
      public String getFullName() {
        return firstName + " " + lastName;
      }
      public String getCommaName() {
        return lastName + ", " + firstName;
      }
    }
    Write a Function (with the appropriate types) that returns true if a given student is "John Smith"
    with a student number of "js123" (otherwise return false).
 */

abstract class Student{
    private final String firstName;
    private final String lastName;
    // NB! Private field:
    private String studentNumber;

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Student(String firstName, String lastName, String studentNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.studentNumber = studentNumber;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }
    public String getCommaName() {
        return lastName + ", " + firstName;
    }
}

class SchoolStudent extends Student{
    public SchoolStudent(String firstName, String lastName, String studentNumber){
        super(firstName, lastName, studentNumber);
    }
}

class UniversityStudent extends Student{
    // NB! public field
    public String studentNumber;

    public UniversityStudent(String firstName, String lastName, String studentNumber){
        super(firstName, lastName);
        this.studentNumber = studentNumber;
    }
}


// Just a demo snippet how to use reflections in order to compare private fields
public class FunctionalProgramming {

    public static boolean isStudentNumberEqual(Student s1, Student s2){
        if(s1 == null && s2 == null) return true;
        if(s1 == null || s2 == null) return false;
        try{
            Field field = Student.class.getDeclaredField("studentNumber");
            field.setAccessible(true);

            Object value1 = field.get(s1);
            Object value2 = field.get(s2);


            return s1.getFullName().equals(s2.getFullName()) && value1.equals(value2);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }


    public static Function<SchoolStudent, Boolean> compareStudents1 = p -> isStudentNumberEqual(p, new SchoolStudent("John", "Smith", "js123"));
    public static Function<UniversityStudent, Boolean> compareStudents2 = p -> "John Smith".equals(p.getFullName()) && "js123".equals(p.studentNumber);

    public static void main(String[] args) {

        SchoolStudent studentPaul = new SchoolStudent("Paul", "Newman", "123");
        SchoolStudent studentJohn = new SchoolStudent("John", "Smith", "js123");
        SchoolStudent studentJack = new SchoolStudent("Jack", "Smith", "js123");

        UniversityStudent uniStudentPaul = new UniversityStudent("Paul", "Newman", "123");
        UniversityStudent uniStudentJohn = new UniversityStudent("John", "Smith", "js123");
        UniversityStudent uniStudentJack = new UniversityStudent("Jack", "Smith", "js123");

        System.out.println(compareStudents1.apply(studentPaul));
        System.out.println(compareStudents1.apply(studentJohn));
        System.out.println(compareStudents1.apply(studentJack));

        System.out.println(compareStudents2.apply(uniStudentPaul));
        System.out.println(compareStudents2.apply(uniStudentJohn));
        System.out.println(compareStudents2.apply(uniStudentJack));
    }
}