package org.example.lambdaexpressions;

import java.util.ArrayList;
import java.util.List;

/**
 * 03.05.24 Katlin Kalde
 * 6. Write a Java program to implement a lambda expression to find the average of a list of doubles.
 */
public class AverageOfListOfDoubles {
    public static void main(String[] args) {
        List<Double> doubles1 = new ArrayList<>(List.of(1.8, 2D, 3D, 5.7, 123D, 45D, 6D, 88D));
        List<Double> doubles2 = new ArrayList<>(List.of(20.0, 30.0, 10.0, 6D));

        double average1 = doubles1.stream()
                .reduce(0.0, (x, y) -> x + y / 2);


        double average2 = doubles2.stream()
                .reduce(0.0, (x, y) -> x + y / 2);

        System.out.println(average1);
        System.out.println(average2);
    }
}
