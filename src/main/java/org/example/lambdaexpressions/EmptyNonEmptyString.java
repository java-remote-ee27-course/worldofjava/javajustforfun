package org.example.lambdaexpressions;

/**
 * 3.05.24. Kätlin Kalde
 * Write a Java program to implement a lambda expression to check if a given string is empty.
 */

interface EmptyOrNot{
    boolean isStringEmpty(String s);
}
public class EmptyNonEmptyString {
    public static void main(String[] args) {
        String string1 = "", string2 = "Blaa";

        EmptyOrNot emptyOrNot = (String str) -> str.isEmpty(); //better: String::isEmpty();
        Boolean findIfEmpty1 = emptyOrNot.isStringEmpty(string1);
        Boolean findIfEmpty2 = emptyOrNot.isStringEmpty(string2);

        System.out.println(findIfEmpty1);
        System.out.println(findIfEmpty2);
    }
}
