package org.example.lambdaexpressions;

import java.util.ArrayList;
import java.util.List;

/**
 * Katlin Kalde 05.2024
 * Write a Java program to implement a lambda expression to convert a list of strings to uppercase and lowercase.
 */



public class UpperCaseAndLowerCase {

    public static List<String> toUpperCase(List<String> strings){
        return strings.stream()
                .map(s -> s.toUpperCase())
                .toList();

    }
    public static List<String> toLowerCase(List<String> strings){
        return strings.stream()
                .map(s -> s.toLowerCase())
                .toList();

    }

    public static List<String> toUpperCase2(List<String> strings){
        return strings.stream()
                .map(String::toUpperCase)
                .toList();

    }
    public static List<String> toLowerCase2(List<String> strings){
        return strings.stream()
                .map(String::toLowerCase)
                .toList();

    }

    public static void main(String[] args) {
        List<String> strings1 = new ArrayList<>(List.of("a", "bBb", "ccc"));
        List<String> strings2 = new ArrayList<>(List.of("A", "Bee", "CEE"));

        System.out.println(toUpperCase(strings1));
        System.out.println(toLowerCase(strings2));
        System.out.println(toUpperCase2(strings1));
        System.out.println(toLowerCase2(strings2));

    }
}
