package org.example.lambdaexpressions;

import java.util.function.BiFunction;

/**
 * Katlin Kalde 05.2024
 * Write a Java program to implement a lambda expression to create a lambda expression to check if a number is prime.
 */
interface FindPrime{
    boolean isPrime(int x);
}
public class PrimeNonPrime {

    public static void main(String[] args) {
        //Version 1:
        System.out.println(isPrime(3));

        //Version 2:
        BiFunction <Integer, Integer, Boolean> isPrimeNumber = (x, y) -> x % y == 0;
        Boolean result = isPrimeNumber.apply(8, 2);
        System.out.println(result);
    }
    private static boolean isPrime(int y){
        FindPrime findPrime = (x) -> (x % 2 == 0);
        return findPrime.isPrime(y);
    }


}
