package org.example.lambdaexpressions;

import com.sun.jdi.IntegerValue;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * Katlin Kalde. 3.05.24
 * 5. Write a Java program to implement a lambda expression to sort a list of strings in alphabetical order.
 */



public class StringsSortAlphabetically {
    public static void main(String[] args) {
        List<String> strings1 = new ArrayList<>();
        strings1.add("Muff");
        strings1.add("Ahaa");
        strings1.add("Str");

        strings1.sort((a, b) -> a.compareTo(b));        //better: String::compareTo
        System.out.println(strings1);
        strings1.add("Muff");
        strings1.add("Ahaa");
        strings1.add("Str");

        strings1.sort(String::compareTo);
        System.out.println(strings1);
    }
}


