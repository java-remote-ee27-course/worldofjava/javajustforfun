package org.example.lambdaexpressions;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Katlin Kalde 04.05.24
 * Write a Java program to implement (stream or a lambda expression) to remove duplicates from a list of integers.
 */

public class RemoveDuplicates {

    public static void main(String[] args) {
        List<Integer> ints = new ArrayList<Integer>(){{ add(1); add(1); add(3); add(5); add(5); }};

        List <Integer> integers = ints
                .stream()
                .distinct()
                .toList();

        System.out.println(integers);

    }

}

