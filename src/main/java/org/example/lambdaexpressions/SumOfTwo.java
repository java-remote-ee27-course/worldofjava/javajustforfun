package org.example.lambdaexpressions;

import static java.lang.Long.sum;

/**
 * Katlin kalde 05.2024
 * Write a Java program to implement a lambda expression to find the sum of two integers.
 */

interface Summative{
    int findSum(int x, int y);
}

public class SumOfTwo {

    public static void main(String[] args) {

        Summative summing = (x,y) -> (x + y); //Summative summing = Integer::sum;
        int result = summing.findSum(2, 3);
        System.out.println(result);
    }
}
