package org.example.io.comparedatafromfiles;

import lombok.*;

import java.io.*;


import java.text.DecimalFormat;
import java.util.*;


/**
 * Katlin Kalde 08-09.06.2024
 * Read spotsmen results from a file, calculate averages:
 * * (1) - of as many results there are present for the current sportsman; [DONE]
 * * (2) - of the best 2 results of each sportsman, [DONE]
 * * (3) - the average of results of the best 3 sportsmen with at least 3 results [DONE]
 * find three best sportsmen (all three results exist + sum of all points is the smallest) [DONE]
 * If there are more than three, i.e some best athlets results equal, find all of them. [NOT DONE]
 * Write the best three results to a new file, each athlete's result to separate row: [DONE]
 * * <nameOfTheSportsman>; <averageOfTwoResults>; <averageOfThreeResults>; <sumOfResults>.
 *
 *  Read spotsmen results from two files, do all the same calculations as before [NOT DONE]
 * TODO:: calculations with athletes from 2 files.
 */
public class CompareDataFromTwoFiles {
    public static void main(String[] args) throws IOException {

        String file1 = "target/classes/file1.txt";
        FromFileReader sportsmenFromFile = new FromFileReader();

        //Averages of all results for each sportsman:
        Statistics statistics = new Statistics(sportsmenFromFile.readFromFileAndCreateSportsmen(file1));
        var averagesOfAllAthletes = statistics.getAthletesResultsAverages();
        System.out.println("Averages of all sportsmen: " + averagesOfAllAthletes);

        //Averages of two results of each sportsman
        Statistics statistics2  = new Statistics(sportsmenFromFile.readFromFileAndCreateSportsmen(file1));
        List <Athlete> allAthletes = statistics2.getAthletes();
        allAthletes.forEach(a -> System.out.println("Averages of 2 results of " +
                a.getName() +
                " " +
                a.getAverageOfAthleteResults(a.getANumberOfAthleteResults(2)
        )));

        //Averages of athletes, who have 3 results, sorted by the athlete (the best is first)
        List<Athlete> athletes = statistics.athletesWithThreeNonZeroResults();
        List<Athlete> sortedAthletesWithThreeResults = statistics.sortAthletesWithThreeResultsByTheirResults(athletes);
        System.out.println("All athletes w 3 results, sorted: " + sortedAthletesWithThreeResults);

        // 3 best athletes
        List <Athlete> bestThreeAthletes = statistics.filterTheBestResults(3, sortedAthletesWithThreeResults);
        System.out.println("Best 3: " + bestThreeAthletes);

        // Statistics of 3 best athletes, starting from the best:
        Statistics statisticsOfThreeBest = new Statistics(bestThreeAthletes);
        var averages = statisticsOfThreeBest.getAthletesResultsAverages();
        System.out.println("Averages of the best three sportsmen: " + averages);
        System.out.println("Summary statistics of three best: " + statisticsOfThreeBest.getStatisticsOfAllAthletes());

        //Print to System.out the results of the three best: name; averageOfTwoResults; averageOfAllResults; sumOfAllResults
        DataPrinter dataPrinter = new DataPrinter();
        bestThreeAthletes.forEach(a -> System.out.println(dataPrinter.calculateResultsForPrinting(a)));

        //Write to file  the results of the three best: name; averageOfTwoResults; averageOfAllResults; sumOfAllResults
        ToFileWriter fileWriter = new ToFileWriter();
        fileWriter.writeToFile("target/classes/fileWithResults.txt", dataPrinter.getAllAthletesForPrinting(bestThreeAthletes).toString());

    }
}

class FromFileReader  {
    public List<Athlete> readFromFileAndCreateSportsmen(String file) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(file));
        List<Athlete> sportsmen = new ArrayList<>();

        String line;
        while((line = reader.readLine())!=null){
            String[] strings = line.split(",");

            Athlete athlete = new Athlete();

            athlete.setName(strings[0]);

            List<Result> results = new ArrayList<>();

            for(int i = 1; i < strings.length; i++){
                results.add(new Result(Double.parseDouble(strings[i])));

            }
            athlete.setResults(results);
            sportsmen.add(athlete);
        }
        return sportsmen;
    }
}

class ToFileWriter{
    public void writeToFile(String file, String data) throws IOException{
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write(data);
        writer.close();

    }
}

class DataPrinter{

    public String calculateResultsForPrinting(Athlete athlete){
        DecimalFormat df = new DecimalFormat("#.##");
        String name = athlete.getName();
        athlete.sortResults();
        List<Result> getTwoBestResults = athlete.getANumberOfAthleteResults(2);
        Double averageOfTwoResults = athlete.getAverageOfAthleteResults(getTwoBestResults);
        Double averageOfAllResults =  athlete.getAverageOfAthleteResults(athlete.getResults());
        double sumOfAllResults = athlete.getSummaryStatistics(athlete.getResults()).getSum();
        return name + ";" + df.format(averageOfTwoResults) + ";" + df.format(averageOfAllResults) + ";" + sumOfAllResults;

    }

    public StringBuilder getAllAthletesForPrinting(List<Athlete> athletes){
        StringBuilder stringBuilder = new StringBuilder();
        athletes.forEach(a -> stringBuilder.append(calculateResultsForPrinting(a)).append("\n"));
        return stringBuilder;
    }
}

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
class Athlete {
    String name;
    List<Result> results;

    public DoubleSummaryStatistics getSummaryStatistics(List<Result> results){
        return results.stream()
                .mapToDouble(Result::getResult)
                .summaryStatistics();
    }
    public Double getAverageOfAthleteResults(List<Result> results){
        return getSummaryStatistics(results).getAverage();
    }

    public void sortResults(){
        //sort starting from the smallest (the best result)
        results.sort(Comparator.comparing(r -> r));
    }

    public List<Result> getANumberOfAthleteResults(int numberOfResults){
        return results.stream().limit(numberOfResults).toList();

    }
}


@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
class Result implements Comparable<Result>{
    Double result;

    @Override
    public int compareTo(Result r) {
        return result.compareTo(r.getResult());
    }
}



@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
class Statistics{
    List <Athlete> athletes;


    public List<List<Result>> getResultsOfAllAthletes(){
        return athletes
                .stream()
                .map(Athlete::getResults)
                .toList();
    }

    public List<DoubleSummaryStatistics> getStatisticsOfAllAthletes (){
        return athletes
                .stream()
                .map(a -> a.getSummaryStatistics(a.getResults()))
                .toList();
    }

    public List<Double> getAthletesResultsAverages(){
        return athletes
                .stream()
                .map(a -> a.getSummaryStatistics(a.getResults()).getAverage())
                .toList();
    }

    public List<Double> getAthletesResultsAveragesNotConsideringZeros(){
        List<Athlete> athletesWithZeroResultsRemoved = new ArrayList<>();

        for(Athlete a : athletes){
            List<Result> resultsWithoutZeros = a.getResults().stream().filter(r -> r.getResult() > 0.0).toList();
            Athlete athlete = new Athlete(a.getName(), resultsWithoutZeros);
            athletesWithZeroResultsRemoved.add(athlete);
        }
        return athletesWithZeroResultsRemoved
                .stream()
                .map(a -> a.getSummaryStatistics(a.getResults()).getAverage())
                .toList();
    }

    public List<Double> getAthletesResultsSums(){
        return athletes
                .stream()
                .map(a -> a.getSummaryStatistics(a.getResults()).getSum())
                .toList();
    }

    public List<Long> getAthletesResultsCounts(){
        return athletes
                .stream()
                .map(a -> a.getSummaryStatistics(a.getResults()).getCount())
                .toList();
    }

    public List<Athlete> athletesWithThreeNonZeroResults(){
        return athletes
                .stream()
                .filter(a -> a.getResults().size() == 3)
                .toList();
    }

    public List<Athlete> sortAthletesWithThreeResultsByTheirResults(List<Athlete> athletes)  {
        List<Athlete> bestAthletes = new ArrayList<>(athletes);
        bestAthletes.sort(Comparator.comparing(a -> a.getSummaryStatistics(a.getResults()).getSum()));
        return bestAthletes;
    }

    public List<Athlete> filterTheBestResults(int number, List<Athlete> allAthletes){
        int i = 0;
        List<Athlete> athletes = new ArrayList<>();
        while(i< number){
            athletes.add(allAthletes.get(i++));
        }
        return athletes;
    }
}