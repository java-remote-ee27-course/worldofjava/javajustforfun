package org.example.io.iostream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Stream;

//read a file, count words in each row
//filter out rows that contain certain words, e.g. "Test"
// Katlin Kalde 05.2024

public class IOStream {
    public static void main(String[] args) throws IOException {
        //File to read from:
        Path path = Path.of("src/main/resources/1.txt");
        //to filter out all lines that contain that word
        String wordWhichLineToFilterOut = "test";
        countWordsInEachRow(path);
        //countWordsInEachRow2(path);
        filterOutCertainRows(path, wordWhichLineToFilterOut);

    }

    //count words in each row in a file (version 1)
    public static void countWordsInEachRow(Path path){
        try(Stream<String> fileLinesStream = Files.lines(path)){
            fileLinesStream.map(l -> l.split(" ").length)
                    .forEach(System.out::println);
        } catch (IOException e){
            System.out.println(e.getMessage() + " " );
        }
    }

    // //count words in each row in a file (version 2):
    public static void countWordsInEachRow2(Path path){
        try(Stream<String> fileLinesStream = Files.lines(path)){
            fileLinesStream.map(l -> Arrays.stream(l.split(" ")).count())
                    .forEach(System.out::println);
        } catch (IOException e){
            System.out.println(e.getMessage() + " " );
        }
    }
    public static void filterOutCertainRows(Path path, String str){
        try(Stream<String> fileLinesStream = Files.lines(path)){
            fileLinesStream
                    .filter(line -> Arrays.stream(line
                                            .split(" "))
                                            .noneMatch(w -> w.equalsIgnoreCase(str)))
                    .forEach(System.out::println);
        } catch (IOException e){
            System.out.println(e.getMessage() + " " );
        }
    }

}
