package org.example.musiclibrary;

import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * 05.2024
 * Katlin Kalde
 *  Write a Java program to create a class called "MusicLibrary"
 *  with a collection of songs and methods to add and remove songs, and to play a random song.
 */

@Data
@AllArgsConstructor
class Song{
    private String title;
    private String genre;
    private String author;
}


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MusicLibrary {
    private List<Song> songs;

    public void playRandomSong(){
        Random random = new Random();
        int songNumber = random.nextInt(songs.size());
        System.out.println("Playing song " + songs.get(songNumber));
    }

    public void addSong (Song song){
        songs.add(song);
    }


    public void removeSongFromList(Song song){
        songs.remove(song);
    }

    public void removeSongFromListByTitle(String songTitle) {
        songs.removeIf(song -> song.getTitle().equalsIgnoreCase(songTitle));
    }

    public List<Song> createNewListWithRemovedSong (String songTitle){
        return songs.stream().filter(s -> !s.getTitle()
                        .equalsIgnoreCase(songTitle))
                .toList();
    }
}



class Main{
    public static void main(String[] args) {
        Song song1 = new Song("Lala", "jazz", "Nat King Cole");
        Song song2 = new Song("Primavera", "classical", "Giuseppe Verdi");
        Song song3 = new Song("Estate", "classical", "Giuseppe Verdi");
        List<Song> songs = new ArrayList<>(List.of(song1, song2, song3));
        MusicLibrary musicLibrary = new MusicLibrary();
        musicLibrary.setSongs(songs);
        musicLibrary.playRandomSong();

        Song song4 = new Song("Non è mai abbastanza", "pop", "Francesco Silvestre");
        Song song5 = new Song("Gioia", "pop", "Francesco Silvestre");
        musicLibrary.addSong(song4);
        musicLibrary.addSong(song5);
        System.out.println("After adding 2 songs: " + musicLibrary);
        musicLibrary.removeSongFromListByTitle("Non è mai abbastanza");
        musicLibrary.removeSongFromList(song5);
        System.out.println("After removal of 2 songs: " + musicLibrary);

        System.out.println("New list with removed song: " + musicLibrary.createNewListWithRemovedSong("Estate"));
        System.out.println("Old list, no song removed now: " + musicLibrary);

    }


}