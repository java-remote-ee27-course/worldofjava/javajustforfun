package org.example.inheritance.shapesandinheritance;

import lombok.*;

/**
 * Katlin kalde 04.2024
 * Class example with inheritance
 * https://www.w3resource.com/java-exercises/index-interface.php
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
abstract class Shape{
    private String color;
    public abstract double getArea();

    public void printArea(){
        System.out.println("The area of the shape is " + getArea());
    }
}

@Getter
@Setter

class Rectangle extends Shape{
    private double side1;
    private double side2;

    public Rectangle() {
        super();
    }


    public Rectangle(double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    public Rectangle(double side1, double side2, String color) {
        super(color);
        this.side1 = side1;
        this.side2 = side2;
    }

    @Override
    public double getArea() {
        return side1 * side2;
    }

    public void printArea(){
        System.out.println("Print inside rectangle child class: " + getArea());
    }
    public void printRectancleArea(){
        System.out.println("The shape is rectangle.");
        super.printArea();
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "side1=" + side1 +
                ", side2=" + side2 +
                "} " + super.toString();
    }
}

class Square extends Rectangle{

    public Square(double side){
        super(side, side);
    }
}

@ToString
class Circle extends Shape{
    private double radius;


    public Circle(){}

    public Circle(double radius) {
        this.radius = radius;
    }


    public Circle(double radius, String color) {
        super(color);
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return radius * radius * Math.PI;
    }
}

public class ShapesAndInheritance {
    public static void main(String[] args) {
        Shape shape = new Rectangle(2, 5, "red");
        System.out.println("First printout shape rectangle 2, 5: ");
        shape.printArea();

        Rectangle rectangle = (Rectangle) shape;
        System.out.println("Second printout rectangle printArea 2, 5: ");
        rectangle.printArea();
        System.out.println("Second printout rectangle printRectangleArea 2, 5: ");
        rectangle.printRectancleArea();

        System.out.println(rectangle);

        Rectangle rectangle1 = new Rectangle();
        System.out.println(rectangle1);

        Circle circle = new Circle(2, "yellow");
        System.out.println(circle);
        Circle circle2 = new Circle();
        circle2.setColor("red");
        System.out.println(circle2);

        Square square = new Square(3);
        System.out.println("PRINT SQUARE AREA : " + square.getArea());
        square.printArea();
        square.setColor("green");
        System.out.println(square);

    }
}
