package org.example.inheritance.personsandemployees;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 03.05.24 Katlin Kalde
 * Write a Java program to create a class known as Person with methods called getFirstName() and getLastName().
 * Create a subclass called Employee that adds a new method named getEmployeeId()
 * and overrides the getLastName() method to include the employee's job title.
 */


@ToString
class Person{
    private final String firstname;
    private final String lastname;

    public Person(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }
}

@ToString
class Employee extends Person{
    private final int id;
    @Getter
    private final String jobTitle;


    public Employee(int id, String firstname, String lastname, String jobTitle){
        super(firstname, lastname);
        this.id = id;
        this.jobTitle = jobTitle;
    }

    public int getEmployeeId(){
        return id;
    }

    @Override
    public String getLastname(){
        return "Last name: " + super.getLastname() + ". Job title: " + getJobTitle() + ".";
    }
}

public class App {
    public static void main(String[] args) {
        Person person = new Person("Mary", "Poppins");
        Employee employee = new Employee(1, "Will", "Grieco", "Clown");

        System.out.println(person);
        System.out.println("Person's last name: " + person.getLastname());
        System.out.println(employee);
        System.out.println("Employee: " + employee.getLastname());
        System.out.println(employee.getLastname() + " Id: " + employee.getEmployeeId());

    }
}
