package org.example.inheritance.company;

/**
 * Katlin Kalde 03.05.24
 * Write a Java program to create a class called Employee with methods called work() and getSalary().
 * Create a subclass called HRManager that overrides the work() method and adds a new method called addEmployee().
 */

public class App {
    public static void main(String[] args) {
        Employee employee = new Employee(6000);
        System.out.println(employee.getSalary());
        employee.work();

        HRManager manager = new HRManager(5500);
        System.out.println(manager.getSalary());
        manager.work();
        manager.addEmployee();

    }


}

class Employee{
    private double salary;

    public Employee(double salary){
        this.salary = salary;
    }

    public void work(){
        System.out.println("Employee is working now");
    }

    public double getSalary(){
        return salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "salary=" + salary +
                '}';
    }
}

class HRManager extends Employee{

    public HRManager(double salary) {
        super(salary);
    }

    @Override
    public void work(){
        System.out.println("HR manager works");

    }

    public void addEmployee(){
        System.out.println("Adding an Employee");
    }

    @Override
    public String toString() {
        return "HRManager{} " + super.toString();
    }
}