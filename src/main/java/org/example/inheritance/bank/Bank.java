package org.example.inheritance.bank;

/**
 * Katlin Kalde 03.05.24
 * Write a Java program to create a class known as "BankAccount" with methods called deposit() and withdraw().
 * Create a subclass called SavingsAccount that overrides the withdraw() method
 * to prevent withdrawals if the account balance falls below one hundred.
 */
public class Bank {
    public static void main(String[] args) {
        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.setBalance(3500);
        System.out.println(savingsAccount);
        savingsAccount.withdraw(3000);
        System.out.println("Balance: " + savingsAccount.getBalance());
        savingsAccount.withdraw(10_000);
        System.out.println("Balance: " + savingsAccount.getBalance());
        savingsAccount.withdraw(450);
        System.out.println("Balance: " + savingsAccount.getBalance());
        savingsAccount.withdraw(10);
        System.out.println("Balance: " + savingsAccount.getBalance());

    }
}

class BankAccount{
    private double balance;


    public void setBalance(double balance){
        this.balance = balance;
    }

    public double getBalance(){
        return balance;
    }

    public void deposit(double amount){
        balance += amount;
    }

    public void withdraw(double amount){
        balance -= amount;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "balance=" + balance +
                '}';
    }
}

class SavingsAccount extends BankAccount{

    @Override
    public void withdraw(double amount) {

         if (amount < 100 || amount > getBalance()){
             System.out.println("Trying to withdraw ... " + amount);
             System.out.println(amount < 100 ?
                     "Balance: " + getBalance() + ". Less than 100 euros on account, cannot withdraw!" :
                     "You have only " + getBalance() + " euros on account. Cannot withdraw more than that!");
        } else {
                super.withdraw(amount);
                System.out.println("Withdrew " + amount);
        }
    }

    @Override
    public String toString() {
        return "SavingsAccount{} " + getBalance();
    }
}