package org.example.flight;

import java.time.LocalDateTime;
import java.util.Locale;

/**
 * Demo of DateTime and formatters 05.2024
 * Katlin Kalde
 */

public class Main {
    public static void main(String[] args) {
        LocalDateTime departure = LocalDateTime.of(2024, 5, 12, 9, 10, 0);
        LocalDateTime arrival = LocalDateTime.of(2024, 5, 12, 10, 20, 0);
        int delay = 65;
        Locale locale = Locale.of("Estonian");
        Flight flight = new Flight(
                "Rome",
                "Lamezia Terme",
                "123AB",
                departure,
                arrival,
                75);

        System.out.println("flight info: " + flight);
        System.out.println("flight info (nice): " + flight.formatAirplaneToString("dd.MM.YYYY hh:mm:ss", Locale.of("Estonian")));

        LocalDateTime updatedArrivalTime = flight.getFlightUpdatedArrivalTime(delay);
        System.out.println("Scheduled arrival time is: " + TimeDisplay.getFormattedDateTime(flight.getArrival(), "dd.MM.YYYY hh:mm:ss", locale));
        System.out.println("Expected arrival time is: " + TimeDisplay.getFormattedDateTime(updatedArrivalTime, "dd.MM.YYYY hh:mm:ss", locale));
    }

}
