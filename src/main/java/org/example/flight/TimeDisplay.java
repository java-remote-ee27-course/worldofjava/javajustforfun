package org.example.flight;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class TimeDisplay {

    public static String getFormattedDateTime(LocalDateTime localDateTime, String pattern, Locale locale) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern).withLocale(locale);
        return localDateTime.format(formatter);
    }

    public static int getMinutesFromHours(int hours) {
        return hours * 60;
    }

    public static int getHoursFromMinutes(int minutes) {
        return minutes / 60;
    }

}
