package org.example.flight;

import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Objects;

/**
 * https://www.w3resource.com/java-exercises/oop/index.php
 * Classes & OOP
 * 12. Write a Java program to create a class called "flight"
 * with a flight number, destination, and departure and arrival time attributes,
 * and methods to check flight status and delay.
 * https://mbavatharany.medium.com/date-and-time-9118eaac95b4
 * https://www.baeldung.com/java-format-zoned-datetime-string
 *
 */

@Getter
@Setter
public class Flight {
    private String origin;
    private String destination;
    private String flightNumber;
    private LocalDateTime departure;
    private LocalDateTime arrival;
    private int delay;



    public Flight() {
    }

    public Flight(String origin, String destination, String flightNumber, LocalDateTime departure, LocalDateTime arrival, int delay) {
        this.origin = origin;
        this.destination = destination;
        this.flightNumber = flightNumber;
        this.departure = departure;
        this.arrival = arrival;
        this.delay = delay;
    }

    public LocalDateTime getFlightUpdatedArrivalTime(int delay) {
        return arrival.plusMinutes(delay);
    }


    public String formatAirplaneToString(String pattern, Locale locale) {
        return "Airplane{" +
                "origin='" + origin + '\'' +
                ", destination='" + destination + '\'' +
                ", flightNumber='" + flightNumber + '\'' +
                ", departure=" + TimeDisplay.getFormattedDateTime(departure, pattern, locale) +
                ", arrival=" + TimeDisplay.getFormattedDateTime(arrival, pattern, locale) +
                ", delay=" + delay +
                ", updatedArrivalTime=" + getFlightUpdatedArrivalTime(delay) +
                '}';
    }

    @Override
    public String toString() {
        return "Airplane{" +
                "origin='" + origin + '\'' +
                ", destination='" + destination + '\'' +
                ", flightNumber='" + flightNumber + '\'' +
                ", departure=" + departure +
                ", arrival=" + arrival +
                ", delay=" + delay +
                '}';
    }
}

