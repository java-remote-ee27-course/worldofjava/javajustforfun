package org.example.palindromefinder;
//Find if a word is palindrome. Katlin Kalde 04.2024
public class BestPalindromeFinder {

    public static void main(String[] args) {
        String word = "abcba1";
        System.out.println(isPalindrome(word));
        String word2 = "abcba";
        System.out.println(isPalindrome(word2));
    }

    public static String isPalindrome(String string) {
        int length = string.length();
        int start = 0;
        int end = length-1;

        while(start < end){
            char beginning = string.charAt(start++);
            char ending = string.charAt(end--);
            if(beginning != ending) return "Not a palindrome";
        }
        return "Palindrome";
    }


}
