package org.example.phonenumber;

import java.util.Arrays;

/**
 * Kata.createPhoneNumber(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 0}) // => returns "(123) 456-7890"
 *
 */
public class PhoneNumber {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        System.out.println(createPhoneNumber(nums));
    }
    public static String createPhoneNumber(int[] numbers) {
        // Your code here!

        StringBuilder phone = new StringBuilder();
        phone.append("(");
        for(int i = 0; i < numbers.length; i++){
            if(i == 3) {
                phone.append(") ");
            }
            if(i == 6) {
                phone.append("-");
            }
            phone.append(numbers[i]);
        }
        return phone.toString();
    }
}

