package org.example.animalinterface;

/**
 * Write a Java program to create a Animal interface
 * with a method called bark() that takes no arguments and returns void.
 * Create a Dog class that implements Animal and overrides bark() to print "Dog is barking".
 * Katlin Kalde 04.2024
 */


interface Animal{
    void bark();
}

class Dog implements Animal{
    @Override
    public void bark(){
        System.out.println("Dog is barking");
    }
}


public class AnimalAppWithInterface {

    private static void barks(Animal animal){
        animal.bark();
    }

    public static void main(String[] args) {
        Dog dog = new Dog();
        barks(dog);
        dog.bark();
    }
}
