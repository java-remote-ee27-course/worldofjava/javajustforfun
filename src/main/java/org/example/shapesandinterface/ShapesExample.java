package org.example.shapesandinterface;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Katlin Kalde 05.2024
 * Write a Java program to create an interface Shape with the getArea() method.
 * Create three classes Rectangle, Circle, and Triangle that implement the Shape interface.
 * Implement the getArea() method for each of the three classes.
 * https://www.w3resource.com/java-exercises/index-interface.php
 */

interface Shape {
    double getArea();
}

@Data
@AllArgsConstructor
class Rectangle implements Shape{
    private double shorterSide;
    private double longerSide;

    @Override
    public double getArea() {
        return shorterSide * longerSide;
    }
}

@Data
@AllArgsConstructor
class Circle implements Shape{
    private double radius;

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }
}


class ShapesExample{
    public static double getShapeArea(Shape shape){
        return shape.getArea();
    }

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(20, 30);
        System.out.println(getShapeArea(rectangle));

        Circle circle = new Circle(10);
        System.out.println(getShapeArea(circle));
    }
}