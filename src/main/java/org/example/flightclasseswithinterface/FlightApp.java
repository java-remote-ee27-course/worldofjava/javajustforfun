package org.example.flightclasseswithinterface;

/**
 * Write a Java program to create an interface Flyable with a method called flyObject().
 * Create three classes Spacecraft, Airplane, and Helicopter that implement the Flyable interface.
 * Implement the flyObject() method for each of the three classes.
 * Katlin Kalde, 04.2024
 */
interface Flyable {
    void flyObject();
}

class Spacecraft implements Flyable {

    @Override
    public void flyObject() {
        System.out.println("Spacecraft is flying.");
    }
}

class Airplane implements Flyable {

    @Override
    public void flyObject() {
        System.out.println("Airplane is flying.");
    }
}

class Helicopter implements Flyable {

    @Override
    public void flyObject() {
        System.out.println("Helicopter is flying.");
    }
}

public class FlightApp {

    private static void flyAnObject(Flyable flyable){
        flyable.flyObject();

        if(flyable.getClass().equals(Helicopter.class) || flyable.getClass().equals(Airplane.class)){
            System.out.println("Distance from the Earth no more than 11.000 meters");
        } else {
            System.out.println("Flight to the outer space.");
        }
    }

    public static void main(String[] args) {
        Helicopter helicopter = new Helicopter();
        Airplane airplane = new Airplane();
        Spacecraft spacecraft = new Spacecraft();

        flyAnObject(helicopter);
        flyAnObject(airplane);
        flyAnObject(spacecraft);

        helicopter.flyObject();
        airplane.flyObject();
        spacecraft.flyObject();
    }
}
