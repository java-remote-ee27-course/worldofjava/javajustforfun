package org.example.datetime;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static java.time.LocalDateTime.now;

/**
 * Different demos of DateTime and formatters 05.2024
 * Katlin Kalde
 */

public class DateTime1 {
    public static void main(String[] args) {
        ZoneId myTimezone = ZoneId.systemDefault();
        System.out.println(myTimezone);
        System.out.println(ZoneId.getAvailableZoneIds());
        ZoneId parisZone = ZoneId.of("Europe/Paris");
        System.out.println(parisZone);
        LocalDateTime dateTime = now();
        ZonedDateTime zonedDateTime = dateTime.atZone(parisZone);
        System.out.println(zonedDateTime);
        ZoneOffset zoneOffset = zonedDateTime.getOffset();
        System.out.println(zoneOffset);

        String formattedLocalTime = dateTime.format(DateTimeFormatter.ISO_LOCAL_TIME);
        System.out.println("Local time here: " + formattedLocalTime);

        ZonedDateTime zdt = ZonedDateTime.of(now(), ZoneId.of("UTC"));
        var formattedZoneTime = zdt.format(DateTimeFormatter.ISO_ZONED_DATE_TIME);
        System.out.println(formattedZoneTime);

        LocalDate currentDate = LocalDate.now();
        LocalTime currentTime = LocalTime.now();
        ZoneId myZone = ZoneId.systemDefault();
        ZonedDateTime zdt2 = ZonedDateTime.of(currentDate, currentTime, myZone);
        System.out.println(zdt2);

        DateTimeFormatter zonedFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm z");
        System.out.println(ZonedDateTime.from(zonedFormatter.parse("31.07.2016 14:15 GMT+02:00")));

        LocalDate date = LocalDate.of(2023, 9, 18);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd, yy: EEE").withLocale(Locale.US);
        String formattedDate = date.format(formatter);
        System.out.println(formattedDate);

    }

}
